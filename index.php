<?php 

// API covid-19 Global
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);
curl_close($curl);

$results = json_decode($result, true);

$countries = $results['Countries'];
$global = $results['Global'];
$totalConfirmed = number_format($global['TotalConfirmed']);
$newConfirmed = number_format($global['NewConfirmed']);
$totalDeath = number_format($global['TotalDeaths']);
$newDeath = number_format($global['NewDeaths']);
$totalRecovered = number_format($global['TotalRecovered']);
$newRecovered = number_format($global['NewRecovered']);


// API Covid-19 Indonesia
$indoApi = curl_init();
curl_setopt($indoApi, CURLOPT_URL, 'https://api.kawalcorona.com/indonesia/provinsi/');
curl_setopt($indoApi, CURLOPT_RETURNTRANSFER, 1);
$ch = curl_exec($indoApi);
curl_close($indoApi);

$arr = json_decode($ch, true);

$temp=[];
foreach($arr as $key) {
    array_push($temp, $key['attributes']);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard Coronavirus</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
    integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="
    crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script></head>
    <body>
        
        <!-- Image and text -->
        <nav class="navbar navbar-secondary bg-secondary">
            <a class="navbar-brand" href="#">
                <i class="fas fa-virus d-inline-block align-top text-light" alt="" loading="lazy" style="font-size: 35px;"></i>
                <small class="font-weight-bold text-light">Data Covid-19</small>
            </a>
        </nav>
        <div class="container-fluid mt-1">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8 border-bottom border-info">
                    <canvas id="confirm"></canvas>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 border-bottom border-info bg-dark">
                    <div class="card border-light bg-transparent mt-1">
                        <h5 class="d-flex justify-content-center text-monospace text-light">Date : <?php echo date('d F Y'); ?></h5>
                    </div>
                    <div class="card bg-transparent border-light mt-2">
                        <h5 class="d-flex justify-content-center text-monospace text-light">Global</h5>
                    </div>
                    <div class="container">
                        <div class="row mt-1">
                            <div class="card bg-transparent border-light col-6">
                                <h5 class="text-light text-monospace" style="text-align: center;">Confirmed</h5>
                                <h6 class="text-primary" style="text-align: center;"><?php echo $totalConfirmed; ?><small>(+<?php echo $newConfirmed; ?>)</small></h6>
                            </div>
                            <div class="card bg-transparent border-light col-6">
                                <h5 class="text-light text-monospace" style="text-align: center;">Deaths</h5>
                                <h6 class="text-danger" style="text-align: center;"><?php echo $totalDeath; ?><small>(+<?php echo $newDeath; ?>)</small></h6>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center mt-1">
                            <div class="card bg-transparent border-light col-sm-8 col-md-6 col-lg-6">
                                <h5 class="text-light text-monospace" style="text-align: center; font-size: 130%;">Recovered</h5>
                                <h6 class="text-success" style="text-align: center;"><?php echo $totalRecovered; ?><small>(+<?php echo $newRecovered; ?>)</small></h6>
                            </div>
                        </div>    
                    </div>
                    <div class="container-fluid mt-3">
                        <h4 class="d-flex justify-content-center text-light text-monospace mb-2">Total Recovered</h4>
                        <canvas id="recovered"></canvas>
                    </div>
                </div>
            </div>           
        </div>
        <div class="container-fluid bg-dark">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8 mt-1">
                    <nav class="navbar navbar-dark d-flex justify-content-center bg-dark border border-info">
                        <h2 class="text-light text-monospace">Global</h2>
                    </nav>
                    <div class="border border-primary" style="overflow-y: scroll; height: 400px">  
                        <table class="table table-striped table-dark">
                            <thead>
                                <tr>
                                    <th class="text-monospace" scope="col">Country</th>
                                    <th class="text-monospace" scope="col">Confirmed</th>
                                    <th class="text-monospace" scope="col">Deaths</th>
                                    <th class="text-monospace" scope="col">Recovered</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($countries as $value): ?>
                                <tr>
                                    <th scope="row"><?php echo $value['Country']; ?></th>
                                    <td class="text-info font-weight-bold"><?php echo $value['TotalConfirmed']; ?></td>
                                    <td class="text-danger font-weight-bold"><?php echo $value['TotalDeaths']; ?></td>
                                    <td class="text-success font-weight-bolder"><?php echo $value['TotalRecovered']; ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 mt-1">
                    <nav class="navbar navbar-dark d-flex justify-content-center bg-dark border border-info">
                        <h2 class="text-light text-monospace">Indonesia</h2>
                    </nav>
                    <div style="overflow-y: scroll; height: 400px">
                    <table class="table table-responsive-sm border border-primary table-striped table-dark">
                        <thead>
                            <tr>
                                <th class="text-monospace" scope="col">Province</th>
                                <th class="text-monospace" scope="col">Confirmed</th>
                                <th class="text-monospace" scope="col">Deaths</th>
                                <th class="text-monospace" scope="col">Recovered</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($temp as $data): ?>
                            <tr>
                                <th scope="row"><?php echo $data['Provinsi']; ?></th>
                                <td class="text-info font-weight-bold"><?php echo $data['Kasus_Posi']; ?></td>
                                <td class="text-danger font-weight-bold"><?php echo $data['Kasus_Meni']; ?></td>
                                <td class="text-success font-weight-bold"><?php echo $data['Kasus_Semb']; ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>    
        
        <script>
            var bar = document.getElementById('confirm').getContext('2d');
            var pie = document.getElementById('recovered').getContext('2d');
            
            var data = $.ajax({
                url: "https://api.covid19api.com/summary",
                cache: false
            })
            
            .done(function (canvas) {
                
                function getCountries(canvas) {
                    var temp_countries=[];
                    
                    canvas.Countries.forEach(function(el) {
                        temp_countries.push(el.Country);
                    })
                    return temp_countries;
                }
                
                function getConfirmed(canvas) {
                    
                    var temp_confirm=[];
                    
                    canvas.Countries.forEach(function(el) {
                        temp_confirm.push(el.TotalConfirmed);
                    })
                    return temp_confirm;
                } 
                
                function getRecovered(canvas) {
                    
                    var temp_recovered=[];
                    
                    canvas.Countries.forEach(function(el) {
                        temp_recovered.push(el.TotalRecovered);
                    })
                    return temp_recovered;
                }
                
                var colors=[];
                
                function getRandomColor() {
                    var r = Math.floor(Math.random() * 225);
                    var g = Math.floor(Math.random() * 225);
                    var b = Math.floor(Math.random() * 225);
                    return "rgb(" + r + "," + g + "," + b + ")";
                }
                
                for (var i in canvas.Countries){
                    colors.push(getRandomColor());
                }
                
                var barChart = new Chart(bar,{
                    type: 'bar',
                    data: {
                        labels: getCountries(canvas),
                        datasets: [{
                            label: 'Total',
                            data: getConfirmed(canvas),
                            backgroundColor: colors,
                            borderWidth: 1
                        }]
                    },
                    options:{
                        legend: {
                            display: false,
                            responsive: true
                        }
                    }
                })
                
                var pieChart = new Chart(pie,{
                    type: 'pie',
                    data: {
                        labels: getCountries(canvas),
                        datasets: [{
                            label: 'Total',
                            data: getRecovered(canvas),
                            backgroundColor: colors,
                            borderWidth: 1
                        }]
                    },
                    options:{
                        legend: {
                            display: false
                        }
                    }
                })
            });
            
        </script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>    
    </body>
    </html>
